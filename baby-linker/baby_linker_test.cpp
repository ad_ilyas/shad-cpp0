#include <catch.hpp>

#include "baby_linker.h"

void PushCode(ObjectFile* file, std::string symbol, std::string code) {
    file->objects.emplace_back(Object{ObjectType::CODE, symbol, file->code_segment.size(), code.size()});
    file->code_segment += code;
}

void PushData(ObjectFile* file, std::string symbol, std::string data) {
    file->objects.emplace_back(Object{ObjectType::RODATA, symbol, file->data_segment.size(), data.size()});
    file->data_segment += data;
}

struct TestReporter : public ErrorReporter {
    std::vector<std::string> UndefinedSymbols;
    std::vector<std::string> DuplicateSymbols;

    virtual void ReportUndefinedSymbol(const std::string& symbol) override {
        UndefinedSymbols.push_back(symbol);
    }

    virtual void ReportMultipleSymbolDefinitions(const std::string& symbol) override {
        DuplicateSymbols.push_back(symbol);
    }
};

TEST_CASE("Object files are always linked") {
    ObjectFile file;
    PushCode(&file, "main", "ret");
    PushData(&file, "kHelloWorld", "Hello world!");

    TestReporter reporter;
    auto exe = Link(std::vector<LinkerArgument*>{&file}, &reporter);

    REQUIRE(reporter.UndefinedSymbols.empty());
    REQUIRE(reporter.DuplicateSymbols.empty());

    REQUIRE(exe);
    REQUIRE(exe->objects.size() == 2);

    REQUIRE(exe->objects[0].type == ObjectType::CODE);
    REQUIRE(exe->objects[0].symbol_name == "main");

    REQUIRE(exe->objects[1].type == ObjectType::RODATA);
    REQUIRE(exe->objects[1].symbol_name == "kHelloWorld");
}