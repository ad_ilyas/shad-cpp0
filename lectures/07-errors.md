# Исключения и RAII

Короткий Фёдор

---
# Обработка ошибок

```c++
int main() {
  auto data = Load();
  Process(&data);
  Save(data);
  return 0;
}
```

 * Что будет, если `Load()` не сможет прочитать ввода?
 * Что будет, если `Process()` не сможет обработать данные?
 * Что будет, если `Save()` не сможет сохранить вывод?

---
# Явная обработка ошибок

Будем возвращать из функции false, если в процессе работы произошла ошибка.

```
std::vector<std::string> Load();
bool Load(std::vector<std::string>* data);

void Process(std::vector<std::string>* data);
bool Process(std::vector<std::string>* data);

void Save(const std::vector<std::string>& data);
bool Save(const std::vector<std::string>& data);
```

---
# Явная обработка ошибок

```c++
int main() {
  std::vector<std::string> data;
  if (!Load(&data)) {
    std::cerr << "Load failed" << std::endl;
    return 1;
  }
  if (!Process(&data)) {
    std::cerr << "Process failed" << std::endl;
  }
  Save(data);
  return 0;
}
```

* Какие проблемы есть в этом коде? (4 штуки)

---
# Явная обработка ошибок

```c++
int main() {
  std::vector<std::string> data;
  if (!Load(&data)) {
    // Failed, but why??????
    std::cerr << "Load failed" << std::endl;
    return 1;
  }
  if (!Process(&data)) {
    std::cerr << "Process failed" << std::endl;
    // BUG
  }
  Save(data); // BUG
  return 0;
}
```

---
# Исключения

```c++
void FailngFunc() {
  std::runtime_error error{"Fail"};
  throw error;
}

void SomeFunc() {
  FailingFunc();
}

int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
```

---
# Исключения

```c++
void FailngFunc() {
  std::runtime_error error{"Fail"};
  throw error;
}
void SomeFunc() {
  SomeOtherFunc1();
  FailingFunc();
}
int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
```

* `SomeFunc` ничего не знает про возникновение и обработку ошибки?

---
# Исключения

 * Где ошибка?

```c++
void SomeFunc() {
  auto ptr = new Object();
  SomeOtherFunc();
  delete ptr;
}
```

---
# Исключения

 * Где ошибка?

```c++
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
```

---
# Memory Leak

```c++
void FailngFunc() {
  std::runtime_error error{"Fail"};
  throw error;
}
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
    std::cerr << ex.what() << std::endl;
    return 1;
  }
  return 0;
}
```

---
# Memory Leak

```
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  OtherCode();
  delete ptr;
}
```

```
void SomeFunc() {
  std::vector<int> x(10);
  FailingFunc();
}
```


---
# Memory Leak

```
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
```

Чем отличаются 2 фрагмента кода?

```
void SomeFunc() {
  std::vector<int> x(10); // data_ = new[10] int;
  FailingFunc();
  // delete data_;
}
```

---
# Memory Leak

```
void SomeFunc() {
  auto ptr = new Object();
  FailingFunc();
  delete ptr;
}
```

```c++
void SomeFunc() {
  std::unique_ptr<Object> ptr =
      std::make_unique<Object>();
  FailingFunc();
}
```

---
# Обработка исключений

```c++
void FailngFunc() {
  throw std::runtime_error("Fail");
}
void SomeFunc() {
  std::vector<int> x(10);
  FailingFunc();
}
int main() {
  try {
    SomeFunc();
  } catch (const std::exception& ex) {
  }
}
```

1. Функция бросает исключение.
2. Runtime находит подходящий `catch` блок.
3. Runtime "раскручивает стек", вызывая деструкторы всех локальных переменных.
4. Управление передаётся в `catch` блок.

---
# Иерархия исключений

```c++
namespace std {
class exception {
    virtual const char* what() = 0;
};

class runtime_error : public exception { /* ... */ };
class logic_error : public exception { /* ... */ };
class invalid_argument : public logic_error {};
} // namespace std
```

---
# Иерархия исключений

```c++
void F() {
  try {
    throw std::runtime_error("FOO");
  } catch (const std::logic_error& ) {
  
  } catch (const std::runtime_error& ) {
    // Land here <---
  } catch (const std::exception& ) {
  
  }
}
```

---
# Иерархия исключений

```c++
class Error {};
class FileNotFound : public Error {};

void F() {
  try {
    File f("my.txt");
    f.Write("foo");
  } catch (const FileNotFound& ) {
    CreateFile("my.txt");
    File f("my.txt");
    f.Write("foo");
  } catch (const Error& ) {
    cerr << "Everything is bad :(" << std::endl;
    throw;
  }
}
```

---
# Почему исключения работают?

 * Ошибка может произойти почти в каждой строчке кода.
   - `IntToString("abc");`
   - `json["key"]`
   - `file.open()`
 * Большинство ошибок не фатальны и не должны приводить к отказу всего процесса.
   - `python repl` не смог прочитать строчку кода?
   - `web` сервер не смог обработать запрос?
 * Ошибки происходят "глубоко" в стеке вызовов, а обрабатываются "наверху".
   - `repl` печатает ошибку пользователю
   - `web` сервер отвечает 500 и логирует ошибку
 * Код обработки ошибок исполняется редко и почти никогда не тестируется.

---
# Какие баги можно посадить при обработке ошибок?

 * Забыть обработать ошибку.
 * Забыть освободить память из за раннего return.
 * Потерять контекст ошибки.
 * Любой другой баг в коде передачи ошибки наверх.




