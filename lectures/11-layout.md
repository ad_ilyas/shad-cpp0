# Лекция 11: Расположение объектов в памяти. Выравнивание

## Лев Высоцкий

---

# План

 1. Как лежат объекты в памяти?
 2. Таблицы виртуальных функций
 3. Выравнивание
 4. Указатели на члены

---

# Структуры в памяти

```c++
struct A {
    int64_t x;
    int y;
    char z;
};

int main() {
    A a;
    assert((char*)(&a.x) == (char*)(&a));
    assert((char*)(&a.y) == (char*)(&a) + 8);
    assert((char*)(&a.z) == (char*)(&a) + 12);
}
```

---

# Структуры в памяти

```c++
struct A {
    int64_t x;
    int y;
    char z;
};

int main() {
    assert(offsetof(A, x) == 0);
    assert(offsetof(A, y) == 8);
    assert(offsetof(A, z) == 12);
}
```

---

# Структуры в памяти

1. Поля лежат в порядке объявления
2. Компилятор может переупорядочить секции с разным доступом (но на практике этого не происходит)

---

# Standard layout type (POD)

1. Одинаковый доступ ко всем полям
2. Нет членов ссылочного типа, виртуальных функций и виртуальных базовых классов
3. Все члены и предки имеют `standard layout type`

---

# Структуры в памяти


```c++
struct A {
    std::string x;
    std::vector<int> y;
};
int main() {
    A a;
    assert((char*)(&a.x) == (char*)(&a));
    assert((char*)(&a.y) == (char*)(&a) + 32);
}
```

---

# Структуры в памяти

```c++
int main() {
    std::vector<int> v = {1,2,3};
    auto* begin = (int**)(&v);
    auto* end = (int**)(&v) + 1;
    *end = *begin + 100;
    // Выводит "100 3"
    std::cout << v.size() << ' ' << v.capacity() << std::endl;
}
```

---

# Структуры в памяти

```c++
int main() {
    std::string s = "WTF";
    *((char*)&s + 19) = '?';
    *((char*)&s + 8) = 4;
    std::cout << s << std::endl; // "WTF?"
}
```

---

# Таблица виртуальных функций

 - Возникает, если в классе или предке есть виртуальная функция
 - Каждому классу соответствует своя таблица
 - Указатель хранится в объекте

---


# Таблица виртуальных функций

```c++
struct A {
    virtual void f() {}
    int64_t x;
};
struct B : public A {
    int64_t y;
};
```
```c++
A a;
B b;
A* a_ptr = &b;
B* b_ptr = static_cast<A*>(a_ptr);

std::cout << sizeof(a) << ' ' << sizeof(b) << '\n' // 16 24
          << &a << ' ' << &a.x << '\n'      // 0x7ffcfbdc59e0 0x7ffcfbdc59e8
          << &b << ' ' << &b.y << '\n'      // 0x7ffcfbdc59c8 0x7ffcfbdc59d8
          << a_ptr << ' ' << b_ptr << '\n'; // 0x7ffcfbdc59c8 0x7ffcfbdc59c8
```

---

# Таблица виртуальных функций


```c++
struct A {
    virtual void f() { std::cout << "A::f()\n"; }
};
struct B : public A {
    virtual void f() { std::cout << "B::f()\n"; }
};

int main() {
    using FunPtr = void (*)(void*);
    A a;
    B b;
    auto fa = (FunPtr)**(void***)&a;
    fa(&b);
    auto fb = (FunPtr)**(void***)&b;
    fb(&b);
}
```

---

# Strict aliasing

Когда можно обращаться к объекту типа `T1` через указатель на `T2` ?
  - Типы *совместимы*
  - Или `T2` — это `char`

**Иначе это UB.**

---

# Выравнивание

```c++
struct A {
    char x;
    int16_t y;
    int z;
    char c;
};
struct B {
    int z;
    int16_t y;
    char x;
    char c;
};
struct C {
    int64_t x;
    char y;
};
```
```c++
std::cout << sizeof(A) << ' ' << sizeof(B) << ' ' << sizeof(C);
```

---

# Выравнивание

```c++
struct A {
    char x;
    char p1[1];
    int16_t y;
    int z;
    char c;
    char p2[3];
};
struct B {
    int z;
    int16_t y;
    char x;
    char c;
};
struct C {
    int64_t x;
    char y;
    char p1[7];
};
```

---

# Типичные выравнивания

| тип     | x86  | x86-64 |
|---------|------|--------|
| char    | 1    | 1      |
| short   | 2    | 2      |
| int16_t | 2    | 2      |
| int     | 4    | 4      |
| int64_t | 4    | 8      |
| double  | 8(4) | 8      |
| T*      | 4    | 8      |

---

# Оператор `alignof`

```c++
struct A {
    char x;
    int16_t y;
    int z;
    char c;
};
struct C {
    int64_t x;
    char y;
};
```
```c++
std::cout << alignof(A) << ' ' << alignof(C) << '\n'          // 4 8
          << offsetof(A, y) << ' ' << offsetof(A, z) << '\n'; // 2 4
```

---

# Оператор `alignas`

```c++
struct A {
    float data[4];
};
struct alignas(16) B {
    float data[4];
};
alignas(64) char cache_line[64];
```
```c++
// 4 16 64
std::cout << alignof(A) << ' ' << alignof(B) << ' '
          << alignof(cache_line) << '\n';
```

---

# `std::aligned_storage`

```c++
template<class T, std::size_t N>
class FixedVector
{
    typename std::aligned_storage_t<sizeof(T), alignof(T)> data[N];
    std::size_t m_size = 0;
    ....
};
```

---


# Упаковка

```c++
struct A {
    char x;
    int16_t y;
    int z;
    char c;
};

#pragma pack(push, 1)
struct B {
    char x;
    int16_t y;
    int z;
    char c;
};
#pragma pack(pop)
```
```c++
std::cout << sizeof(A) << ' ' << sizeof(B) << '\n' // 12 8
```

---

# Указатели на члены

 - Объявление: `int A::* ptr = &A::x`
 - Использование: `a.*ptr` или `a_ptr->*ptr`.

