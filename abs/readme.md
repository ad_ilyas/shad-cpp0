# Abs

Это задача типа [crashme](https://best-cpp-course-ever.ru/prime/shad-cpp/blob/master/crash_readme.md).

Исходный код находится в файле abs.cpp. Исполяемый файл получен командой
```
g++ -std=c++14 -O2 -Wall -fsanitize=undefined -fno-sanitize-recover abs.cpp -o abs
```
