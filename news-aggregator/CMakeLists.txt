find_package(Boost 1.54)

if (Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})

    add_catch(test_news_aggregator PRIVATE_TESTS test.cpp)
endif()
